from calculo_intersismico import Intersismico

if __name__ == "__main__":
    params = {
        'zona':'Chile',
        'limites_zona': (-18,-45),
        'stations': []
    }
    intersismico = Intersismico(**params)
    map_params = {
        'latmin': -48,
        'latmax': -15,
        'lonmin': -80,
        'lonmax': -65,
        "paralelos": [-69, -70, -71, -72, -73, -74, -75, -76],
        "meridianos": [-34, -29],
    }
    intersismico.set_map_params(**map_params)
    intersismico.build_map()
    print("Planos de falla data")
    intersismico.plano_falla_ab()
    intersismico.plano_falla_cd()
    intersismico.plano_falla_e()
    print("Construyendo datos")
    intersismico.construye_planos()
    print("Haciendo inversion")
    intersismico.calcula_inversion()
    intersismico.cons
    

    
